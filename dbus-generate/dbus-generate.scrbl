#lang scribble/base

@require[(for-label dbus-generate racket)]

@title{D-Bus interface generation}

@; @defmodule[dbus-generate]

@; @section{Interface generation}

@;{
#lang scribble/base

@title{On the Cookie-Eating Habits of Mice}

If you give a mouse a cookie, he's going to ask for a
glass of milk.}
